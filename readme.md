# DPTI Email Templates

This library is a build tool for creating DPTI email templates. The tool requires basic knowledge of MJML and Twig. All source files live in the "source" folder. The twig files in the root of this directory will be built in to a "build" folder as inlined html.

The `assets` folder under `source` are automatically copied to the `build` folder for ease of internal build/testing. Live templates will require a CDN/hosted solution for referenced assets (ie images & fonts). Currently the `assets_root` variable in `source/templates/layouts/base.twig` sets the root path for all assets.


## Requirements
- Node 10+ 

## How to build
`npm run build`

## Development
`npm run start`
